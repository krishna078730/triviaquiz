import { QuestionType } from "../types/types"
export const constantText = {
  welcome:"Welcome To",
  trivia:"Trivia Quiz",
  question:"Question No:",
  score:"Marks:",
  correct_answer:"Coreect Answer",
  submit:"Check",
  next:"Next Question",
  result:"Show Result",
}

export const emptyQuestion :QuestionType={
    category: '',
    correct_answer: '',
    difficulty: '',
    incorrect_answers: [],
    question: '',
    type: ''
  }
