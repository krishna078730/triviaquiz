import axios from 'axios';

export const getQuestionAPI = async (page: number, callback: any) => {
  await axios
    .get(`https://opentdb.com/api.php?amount=${page}`)
    .then(response => {
      callback(response.data);
    });
};
