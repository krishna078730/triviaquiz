import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  
} from 'react-native';
import { constantText } from './src/constant/default';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import {QuestionsScreen} from './src/components/QuestionsScreen';


const App = (props:any) => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        backgroundColor={"#38adba"}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
         <View style={styles?.container}>
            <Text style={styles?.headertext}>{constantText.welcome} {constantText.trivia}</Text>
        </View>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.green : Colors.grey,
          }}>
          <QuestionsScreen />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create<any>({
  container: {
      height:80,
      backgroundColor:'#38adba',
      justifyContent: 'center',
      paddingLeft:12,
  },
  headertext: {
      fontSize: 30,
      width:350,
      fontWeight: '900', 
      color:'white'
  }
})


